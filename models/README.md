# Message Models
This directory contains JSON files representing the models
of the messages. These files can be used by other
languages to know how to decode the JSON messages
sent to and received from the server.

Created with [StopLight Studio].

[StopLight Studio]: https://stoplight.io/studio/