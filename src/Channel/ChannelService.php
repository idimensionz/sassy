<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Channel;

use iDimensionz\AppServer\Message\Base\TextMessage;
use iDimensionz\AppServer\Service\AbstractServiceMessage;
use iDimensionz\AppServer\Service\AbstractService;
use iDimensionz\AppServer\Channel\ChannelManager;

class ChannelService extends AbstractService
{
    public const ACTION_CREATE = 'create';
    public const ACTION_LIST = 'list';
    public const ACTION_SUBSCRIBE = 'subscribe';
    public const ACTION_SWITCH = 'switch';

    public static string $serviceName = 'topic';

    /**
     * @inheritDoc
     */
    public function execute(AbstractServiceMessage $message, array $parameters = [])
    {
        $from = $message->getFrom();
        switch ($message->action) {
            case self::ACTION_CREATE:
                $channelName = trim($parameters[0]);
                if (!empty($channelName)) {
                    $channel = ChannelManager::getInstance($channelName);
                    $channel->subscribe($from);
                    $content = "New topic '$channel' was created";
                } else {
                    $content = "Channel '$channelName' is not a valid topic name";
                }
                break;
            case self::ACTION_LIST:
                $channelList = ChannelManager::list();
                $content = !empty($channelList) ? implode(PHP_EOL, $channelList) : 'No topics created yet.';
                break;
            case self::ACTION_SUBSCRIBE:
                $channelName = trim($parameters[0]);
                $resultModel = ChannelManager::subscribe($from, $channelName);
                $content = $resultModel->isSuccess ? $resultModel->returnValue : $resultModel->getErrorMessages()[0];
                break;
            case self::ACTION_SWITCH:
                $channelName = trim($parameters[0]);
                $resultModel = ChannelManager::switch($from, $channelName);
                $content = $resultModel->isSuccess ? $resultModel->returnValue : $resultModel->getErrorMessages()[0];
                break;
            default:
                $content = sprintf("Channel action '%s' is not valid.", $message->action);
                break;
        }
        $responseMessage = new TextMessage($from, $content);
        $this->getAppServer()->debug($content);
        $from->send($responseMessage->getEncodedMessage());
//        $encodedChatMessage = $this->getAppServer()->createEncodedSystemChatMessage($message);
//        $this->getAppServer()->distributeEncodedChatMessage($from, $encodedChatMessage, false);
//        $this->getAppServer()->getClients()->offsetSet($from);
    }
}
