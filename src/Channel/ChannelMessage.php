<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Channel;

use iDimensionz\AppServer\Service\AbstractServiceMessage;

class ChannelMessage extends AbstractServiceMessage
{
    public const MESSAGE_TYPE = __CLASS__;
    public const TOPIC_SEPARATOR = '.';

    // Base topic can be used to build a topic hierarchy in child classes.
    // (i.e. 'server.some-game') and then topic can be a specific "channel" in that particular game on that particular
    // server
    public string $baseTopic = '';
    public string $topic;
    public ?string $serviceName = ChannelService::class;

    public function process(): void
    {
        parent::process();
        $this->service->execute($this, $this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $commandData = parent::jsonSerialize();
        $fullyQualifiedTopic = sprintf('%s' . self::TOPIC_SEPARATOR . '%s', $this->baseTopic, $this->topic);
        $topicData = [
            'topic' => $fullyQualifiedTopic,
        ];
        return array_merge($commandData, $topicData);
    }
}
