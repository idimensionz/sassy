<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Channel;

use iDimensionz\AppServer\Channel\ChannelService;
use iDimensionz\AppServer\FactoryInterface;
use iDimensionz\AppServer\Traits\DebugTrait;
use iDimensionz\AppServer\Traits\FactoryTrait;
use iDimensionz\Common\Result\ResultModel;
use iDimensionz\Common\Result\ResultModelTrait;
use Ratchet\ConnectionInterface;

// Contains the functions associated with the actions that the service provides in the execute() function.
class ChannelManager implements FactoryInterface
{
    use DebugTrait;
    use ResultModelTrait;
    use FactoryTrait;

    // Name of the default channel that is created when the server starts.
    public const CHANNEL_DEFAULT = 'ServerDefault';

    public static function getInstance(string $name, ?Channel $parent = null, bool $isPublic = true): Channel
    {
        // @todo Implement "parent" channels.
        if (!self::exists($name)) {
            self::$validInstances[$name] = new Channel($name, $parent, $isPublic);
        }

        return self::$validInstances[$name];
    }

    public static function exists(string $name): bool
    {
        return self::isValidInstance($name);
    }

    public static function find(string $channelName): ?Channel
    {
        // Currently, returns the Server channel if the requested channel is not found.
        // Should it throw an exception instead?
        return self::exists($channelName) ?
            self::$validInstances[$channelName] :
            self::$validInstances[static::CHANNEL_DEFAULT];
    }

    public static function list(): array
    {
        return array_keys(self::$validInstances);
    }

    public static function subscribe(ConnectionInterface $from, string $channelName): ResultModel
    {
        if (static::exists($channelName)) {
            $channel = self::find($channelName);
            if (!$channel->isSubscribed($from)) {
                $channel->subscribe($from);
                $resultModel = self::successResultWithReturnValue("You're now subscribed to '$channelName'");
            } else {
                $resultModel = self::successResultWithReturnValue("You're already subscribed to '$channelName'");
            }
            // Set the connection's current channel to the channel they just subscribed to.
            $from->currentChannel = $channel;
        } else {
            $message = "Channel '$channelName' does not exist.";
            self::debug($message);
            $resultModel = self::failureResultWithMessage($message);
        }

        return $resultModel;
    }

    // Switch's the user's current topic
    public static function switch(ConnectionInterface $from, string $channelName): ResultModel
    {
        if (static::exists($channelName)) {
            $topic = self::find($channelName);
            if ($topic->isSubscribed($from)) {
                $from->currentChannel = $topic;
                $resultModel = self::successResultWithReturnValue("You're now in topic '$channelName'");
            } else {
                $resultModel = self::failureResultWithMessage("You're not subscribed to '$channelName'");
            }
        } else {
            $message = "Channel '$channelName' does not exist.";
            self::debug($message);
            $resultModel = self::failureResultWithMessage($message);
        }

        return $resultModel;
    }
}
