<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer;

interface FactoryInterface
{
    public static function isValidClass(string $className): bool;

    public static function isValidInstance(string $instanceName): bool;

    public static function registerClass(string $className);

    public static function getValidClasses(): array;

    public static function getValidInstances(): array;

    /**
     * @param string $instanceInfo Whatever data the factory needs to create the instance.
     * @return mixed
     */
    public static function getInstance(string $instanceInfo);
}