<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Subscriber;

use iDimensionz\AppServer\Event\Server\RegisterMessagesEvent;
use iDimensionz\AppServer\Message\Base\ImageUrlMessage;
use iDimensionz\AppServer\Message\Base\SystemMessage;
use iDimensionz\AppServer\Message\Base\TextMessage;
use iDimensionz\AppServer\Message\Base\VideoUrlMessage;
use iDimensionz\AppServer\Message\MessageFactory;
use iDimensionz\AppServer\Channel\ChannelMessage;
use iDimensionz\AppServer\Service\ServiceFactory;
use iDimensionz\AppServer\Channel\ChannelService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ServerRegisterMessagesSubscriber implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            RegisterMessagesEvent::NAME => 'registerMessages',
        ];
    }

    public function registerMessages()
    {
        MessageFactory::registerClass(SystemMessage::class);
        MessageFactory::registerClass(TextMessage::class);
        MessageFactory::registerClass(ImageUrlMessage::class);
        MessageFactory::registerClass(VideoUrlMessage::class);
        MessageFactory::registerClass(ChannelMessage::class);
        // @todo move the service registration to a separate event/subscriber.
        ServiceFactory::registerClass(ChannelService::class);
    }
}
