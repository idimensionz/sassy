<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Message\Base;

class VideoUrlMessage extends AbstractUrlMessage
{
    public const MESSAGE_TYPE = self::class;

    public function process(): void
    {
        $this->publishMessageOnTopic($this);
    }
}
