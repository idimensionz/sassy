<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Message\Base;

use Ratchet\ConnectionInterface;

abstract class AbstractUrlMessage extends TextMessage
{
    public const MESSAGE_TYPE = self::class;

    private string $url;
    private array $urlComponents;

    public function __construct(?ConnectionInterface $from = null, string $url = '')
    {
        parent::__construct($from);
        $this->url = $url;
        $urlComponents = parse_url($url);
        $this->urlComponents = is_array($urlComponents) ? $urlComponents : [];
    }

    public function process(): void
    {
        parent::process();
        $this->publishMessageOnTopic($this);
    }

    public function getScheme(): ?string
    {
        return $this->urlComponents['scheme'] ?? null;
    }

    public function getHost(): ?string
    {
        return $this->urlComponents['host'] ?? null;
    }

    public function getPath(): ?string
    {
        return $this->urlComponents['path'] ?? null;
    }

    public function getQuery(): ?string
    {
        return $this->urlComponents['query'] ?? null;
    }

    public function jsonSerialize(): array
    {
        $messageValues = parent::jsonSerialize();
        return array_merge(
            $messageValues,
            [
                'url' => $this->url,
            ],
            $this->urlComponents
        );
    }
}
