<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Traits;

trait DebugTrait
{
    public static function debug($message)
    {
        if (1 == getenv('SASSY_SERVER_DEBUG')) {
            echo $message . PHP_EOL;
        }
    }
}