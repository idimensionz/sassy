<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Event\Server;

use Symfony\Contracts\EventDispatcher\Event;

class RegisterMessagesEvent extends Event
{
    public const NAME = 'server.register.messages';
}
