<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer;

use iDimensionz\AppServer\Traits\DebugTrait;
use iDimensionz\AppServer\Traits\FactoryTrait;

abstract class AbstractFactory implements FactoryInterface
{
    use DebugTrait;
    use FactoryTrait;
}
