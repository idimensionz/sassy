<?php

/*
 * Copyright (c) 2022 by iDimensionz. All rights reserved.
 */

namespace iDimensionz\AppServer\Service;

use iDimensionz\AppServer\Message\AbstractBaseMessage;
use iDimensionz\AppServer\Message\Exception\InvalidMessageTypeException;
use iDimensionz\AppServer\Service\ServiceInterface;
use Ratchet\ConnectionInterface;

class AbstractServiceMessage extends AbstractBaseMessage
{
    public ?string $serviceName = null;
    public ?string $action = null;
    public array $parameters = [];
    protected ?ServiceInterface $service;

    /**
     * @throws InvalidMessageTypeException
     */
    public function __construct(?ConnectionInterface $from, ?ServiceInterface $service = null)
    {
        parent::__construct($from);
        $this->service = $service;
    }

    public function setService(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'serviceName' => $this->serviceName,
            'action' => $this->action,
            'parameters' => $this->parameters,
        ];
    }
}
